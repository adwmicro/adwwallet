import Vue from 'vue'
import Router from 'vue-router'
import Expenses from '@/components/Expenses'
import Home from '@/components/Home'
import Profile from '@/components/Profile'
import Revenues from '@/components/Revenues'
import AddTransaction from '@/components/AddTransaction'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/new',
      name: 'AddTransaction',
      component: AddTransaction
    },
    {
      path: '/expenses',
      name: 'Expenses',
      component: Expenses
    },
    {
      path: '/revenues',
      name: 'Revenues',
      component: Revenues
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    }
  ]
})
