import firebase from './FirebaseAdapter'
import Vue from 'vue'
import {CATEGORIES, TRANSACTIONS} from '@/store/mutation-types'
import {SYNC_CATEGORIES, SYNC_SETTINGS, SYNC_TRANSACTIONS, NEW_TRANSACTION, REMOVE_TRANSACTION} from '@/store/action-types'

const state = {
  settings: null,
  categories: null,
  transactions: null
}

const actions = {
  [SYNC_CATEGORIES] ({commit}) {
    const categories = firebase.database().ref('Categories')
    categories.on('value', data => {
      if (data) {
        commit('setCategories', {
          categories: data.val()
        })
      }
    })
  },
  [SYNC_TRANSACTIONS] ({commit}, user) {
    if (user) {
      const transactions = firebase.database().ref(`Transactions/Users/${user.userId}`)
      transactions.on('value', data => {
        if (data) {
          commit('setTransactions', {
            transactions: data.val()
          })
        }
      })
    }
  },
  [SYNC_SETTINGS] () {
    const settings = firebase.database().ref('Settings')
    settings.on('value', data => {
      if (data) {
        state.settings = data.val()
      }
    })
  },
  [REMOVE_TRANSACTION] (state, payload) {
    if (payload) {
      const transaction = firebase.database().ref(`Transactions/Users/${payload.userId}/${payload.type}/${payload.year}/${payload.month}/${payload.day}`)
      transaction.child(payload.id).remove()
    }
  },
  [NEW_TRANSACTION] (state, payload) {
    if (payload) {
      const transaction = {
        title: payload.title,
        amount: payload.amount,
        category: payload.category,
        type: payload.type,
        date: payload.date
      }
      const year = transaction.date.getFullYear()
      const month = transaction.date.getMonth() + 1
      const day = transaction.date.getUTCDate()

      const connection = firebase.database().ref(`Transactions/Users/${payload.userId}/${payload.type}/${year}/${month}/${day}`)
      connection.push(transaction)
    }
  }
}

const mutations = {
  [CATEGORIES] (state, payload) {
    const categories = [
      {
        text: 'Wybierz kategorię',
        value: '0'
      }
    ]
    Object.keys(payload.categories).map((k, v) => {
      categories.push({ text: k, value: parseInt(v) + 1 })
    })
    Vue.set(state, 'categories', categories)
  },
  [TRANSACTIONS] (state, payload) {
    Vue.set(state, 'transactions', payload)
  }
}

const getters = {
  Database (state) {
    return state
  },
  Categories (state) {
    return state.categories
  },
  Transactions (state) {
    return state.transactions
  }
}

export default {
  actions,
  getters,
  mutations
}
