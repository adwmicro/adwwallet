import firebase from 'firebase'

firebase.initializeApp(process.env['firebase'])

export default firebase
