import firebase from './FirebaseAdapter'
import Database from './Database'
import {AUTH, SIGN_IN, SIGN_OUT, SYNC_TRANSACTIONS} from '@/store/action-types'

const session = {
  isUserLogged: null,
  isSessionChecked: null,
  authUser: null,
  email: null,
  userId: null,
  name: null
}

export const actions = {
  [SIGN_IN] () {
    const provider = new firebase.auth.GoogleAuthProvider()
    firebase.auth().signInWithPopup(provider)
      .catch(error => console.log('Error form firebase: ', error))
  },
  [SIGN_OUT] () {
    firebase.auth().signOut()
      .catch(error => console.log('Error form firebase: ', error))
  },
  [AUTH] () {
    firebase.auth().onAuthStateChanged(user => {
      session.authUser = user
      session.isSessionChecked = true
      if (user) {
        session.isUserLogged = true
        session.name = user.displayName
        session.userId = user.uid
        session.email = user.email
        Database.actions[SYNC_TRANSACTIONS](user.uid)
      } else {
        session.isUserLogged = false
      }
    })
  }
}

const getters = {
  User () {
    return session
  }
}

export default {
  actions,
  getters
}
