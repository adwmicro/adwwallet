// Mutation types only for storing synchronous
// https://vuex.vuejs.org/guide/structure.html

export const CATEGORIES = 'setCategories'
export const TRANSACTIONS = 'setTransactions'
