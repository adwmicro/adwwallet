// Mutation types only for storing asynchronous
// https://vuex.vuejs.org/guide/structure.html
export const SYNC_CATEGORIES = 'synchronizeCategories'
export const SYNC_SETTINGS = 'synchronizeSettings'
export const SYNC_TRANSACTIONS = 'synchronizeTransactions'
export const NEW_TRANSACTION = 'newTransaction'
export const REMOVE_TRANSACTION = 'removeTransaction'
export const AUTH = 'auth'
export const SIGN_IN = 'signInWithGoogle'
export const SIGN_OUT = 'signOut'
