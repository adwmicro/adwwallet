'use strict'
const merge = require('webpack-merge')
const api = require('./api.key')

module.exports = merge(api, {
  NODE_ENV: '"production"'
})
