'use strict'
module.exports = {
  firebase: JSON.stringify({
    apiKey: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    authDomain: 'XXXXXXXX.firebaseapp.com',
    databaseURL: 'https://XXXXXXXXXXXX.firebaseio.com',
    projectId: 'XXXXXXXXXXXX',
    storageBucket: 'XXXXXXXXXXXXXX.appspot.com',
    messagingSenderId: 'XXXXXXXXXXXX'
  })
}
